"use strict"


// Теоретичне питання
// Поясніть своїми словами, як ви розумієте, що таке деструктуризація і навіщо вона потрібна?

// Деструктуризация помогает нам в упрощенном виде получать доступ к данным в обьектах, масивах и функциях. 
// С ее помощью можно легко переназначить переменные, обменять значения обьединить несколько обьектов или массивов
//  в один. Универсальное свойство, много полезного.

// __________________________________________________________________________________________________
// Завдання 1

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон", "Гилберт"];

let newCompany = Array.from(new Set([...clients1, ...clients2]));
console.log(newCompany);

// _____________________________________________________________________________________________________
// Завдання 2

// const characters = [
// 	{
// 		name: "Елена",
// 		lastName: "Гилберт",
// 		age: 17,
// 		gender: "woman",
// 		status: "human"
// 	},
// 	{
// 		name: "Кэролайн",
// 		lastName: "Форбс",
// 		age: 17,
// 		gender: "woman",
// 		status: "human"
// 	},
// 	{
// 		name: "Аларик",
// 		lastName: "Зальцман",
// 		age: 31,
// 		gender: "man",
// 		status: "human"
// 	},
// 	{
// 		name: "Дэймон",
// 		lastName: "Сальваторе",
// 		age: 156,
// 		gender: "man",
// 		status: "vampire"
// 	},
// 	{
// 		name: "Ребекка",
// 		lastName: "Майклсон",
// 		age: 1089,
// 		gender: "woman",
// 		status: "vempire"
// 	},
// 	{
// 		name: "Клаус",
// 		lastName: "Майклсон",
// 		age: 1093,
// 		gender: "man",
// 		status: "vampire"
// 	}
// ];


// let charactersShortInfo = [];

// for(let i = 0; i < characters.length; i++) {
// 	charactersShortInfo[i] = changeCharacters (characters[i]);
// }

// function changeCharacters ({name, lastName, age}) {
// 	let newPerson = {name, lastName, age};
// 	return newPerson;
// 	}

// console.log(charactersShortInfo);



// __________________________________________________________________________________________________________
// Завдання 3

// const user1 = {
// 	name: "John",
// 	years: 30
// };

// const { name, years: age, isAdmin = false } = user1;

// console.log(name);
// console.log(age);
// console.log(isAdmin);


// ___________________________________________________________________________________________________________
// Завдання 4

// const satoshi2020 = {
// 	name: 'Nick',
// 	surname: 'Sabo',
// 	age: 51,
// 	country: 'Japan',
// 	birth: '1979-08-21',
// 	location: {
// 		lat: 38.869422,
// 		lng: 139.876632
// 	}
// }

// const satoshi2019 = {
// 	name: 'Dorian',
// 	surname: 'Nakamoto',
// 	age: 44,
// 	hidden: true,
// 	country: 'USA',
// 	wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
// 	browser: 'Chrome'
// }

// const satoshi2018 = {
// 	name: 'Satoshi',
// 	surname: 'Nakamoto',
// 	technology: 'Bitcoin',
// 	country: 'Japan',
// 	browser: 'Tor',
// 	birth: '1975-04-05'
// }

// const fullProfile = { ...satoshi2018, ...satoshi2019, ...satoshi2020 };

// console.log(fullProfile);


// ________________________________________________________________________________________________
// Завдання 5

// const books = [{
// 	name: 'Harry Potter',
// 	author: 'J.K. Rowling'
// }, {
// 	name: 'Lord of the rings',
// 	author: 'J.R.R. Tolkien'
// }, {
// 	name: 'The witcher',
// 	author: 'Andrzej Sapkowski'
// }];

// const bookToAdd = {
// 	name: 'Game of thrones',
// 	author: 'George R. R. Martin'
// }

// const newBooks = [...books, bookToAdd]

// console.log(newBooks);


// _________________________________________________________________________________________________
// Завдання 6

// const employee = {
// 	name: 'Vitalii',
// 	surname: 'Klichko'
// }

// const newEmployee = {...employee, age: "", salary: ""}

// console.log(newEmployee);



// _______________________________________________________________________________________________
// Завдання 7

// const array = ['value', () => 'showValue'];

// 1вариант
// const [value, showValue] = array;

// 2вариант
// let value = array[0];
// function showValue() {
// 	return array[1]
// }

// alert(value); // має бути виведено 'value'
// alert(showValue());  // має бути виведено 'showValue'

