"use strict"

// Теоретичне питання
// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript?

// AJAX это технология или точнее подход, позволяющий нам без перезагрузки страницы
//  получать и изменять данные, делая это асинхронно (вне потока)


let body = document.querySelector('body');

class Film {
	constructor(value) {
		this.name = value.name;
		this.episodeId = value.episodeId;
		this.openingCrawl = value.openingCrawl;
		this.characters = value.characters;
	}

	showFilm() {
		let ul = document.createElement('ul');
		ul.innerHTML = `<strong>MOVIE:<strong>`
		body.append(ul);

		let liName = document.createElement('li');
		let liEpisodeId = document.createElement('li');
		let liOpeningCrawl = document.createElement('li');

		liName.innerHTML = `<strong>${this.name}<strong>`;
		liEpisodeId.innerText = `Episode: ${this.episodeId}`;
		liOpeningCrawl.innerText = this.openingCrawl;

		ul.append(liName);
		ul.append(liEpisodeId);
		ul.append(liOpeningCrawl);

		this.showCharacters(liName);
	}

	async showCharacters(elemDom) {

		let ul = document.createElement('ul');
		// ul.innerText = `Actors:`;
		ul.innerHTML = `<div class="loader"></div>
								<ul><strong>Actors:<strong></ul>`;
		elemDom.append(ul);
		let div = document.querySelectorAll('.loader');

		for (let i = 0; i < this.characters.length; i++) {
			const result = await fetch(this.characters[i]);
			await result.json().then(res => {
				let person = document.createElement('li');
				person.innerText = `${i + 1}: ${res.name}`;
				ul.append(person);

				if (i >= this.characters.length - 1) {
					div.forEach(el => {
						el.classList.add('none');
					})
				}
			});
		}
	}
}

async function getFilm() {
	const result = await fetch("https://ajax.test-danit.com/api/swapi/films");
	return await result.json().then(res => {

		for (let i = 0; i < res.length; i++) {
			let obj = res[i];

			const films = new Film(obj).showFilm();
		}
	})
}


getFilm();
