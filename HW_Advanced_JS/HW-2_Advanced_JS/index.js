"use strict"

// Теоретичне питання:
// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
		// При получении данных с сервера, мы можем себя перестраховатьб так как они могут прийти не коректно или так же при валидации пароля.
		//  Но в общем сложно навести какие-то еще примеры, все в момент отладки, если мы видим, что куда-то может прийти ошибка, 
		//  тогда что-бы код не ложился без сообщений, мы будем использовать try...catch.




const books = [
	{
		author: "Люсі Фолі",
		name: "Список запрошених",
		price: 70
	},
	{
		author: "Сюзанна Кларк",
		name: "Джонатан Стрейндж і м-р Норрелл",
	},
	{
		name: "Дизайн. Книга для недизайнерів.",
		price: 70
	},
	{
		author: "Алан Мур",
		name: "Неономікон",
		price: 70
	},
	{
		author: "Террі Пратчетт",
		name: "Рухомі картинки",
		price: 40
	},
	{
		author: "Анґус Гайленд",
		name: "Коти в мистецтві",
	}
];




class Book {

	constructor(author, name, price) {

		try {
			if (!author) {
				throw new Error(`Error, author undefined`);
			} else if (!name) {
				throw new Error(`Error, name undefined`);
			} else if (!price) {
				throw new Error(`Error, price undefined`);
			}

			this.author = author
			this.name = name
			this.price = price

		} catch (err) {
			console.log(err);
		}
	}

	render() {
		const parent = document.querySelector("#root");
		const wrapper = document.createElement('ul');

		for(let prop in this) {
			let elem = document.createElement('li');
			elem.innerText = this[prop];
			wrapper.append(elem);
		}
		parent.append(wrapper);
	}
}


for (let obj of books) {
	new Book(obj.author, obj.name, obj.price).render();
}

