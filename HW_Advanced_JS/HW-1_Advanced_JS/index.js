"use strict"

// Теоретичне питання
//   1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
//   2. Для чого потрібно викликати super() у конструкторі класу-нащадка?

// Ответы:
// 1. Все что есть у родителя, передается и доступно наследнику или всем наследникам, которые есть в цепочке.
// 	Но то что есть у наследников, не доступно для родителя.

// 2. super()  в конструкторе необходимо вызывать, что бы передать или обратиться к свойствам родителя.


class Employee {
	constructor(name, age, salary) {
		this.name = name,
			this.age = age,
			this.salary = salary;
	}
	get newName() {
		return this.name
	}
	set newName(name) {
		return this.name
	}

	get newAge() {
		return this.age
	}
	set newAge(age) {
		return this.age
	}

	get newSalary() {
		return this.salary;
	}
	set newSalary(salary) {
		return this.salary;
	}
}

class Programmer extends Employee {
	constructor(name, age, salary, lang) {
		super(name, age, salary);
		this.lang = lang;
	}
	get newSalary() {
		return this.salary * 3;
	}
}

const alex = new Programmer('Alex', 33, 1000, 'html, css, js');
const artem = new Programmer('Artem', 21, 2000, 'js');
const igor = new Programmer('Igor', 21, 3000, 'English');

console.log(alex, artem, igor);
console.log(`${alex.name} salary: ${alex.newSalary}`);
console.log(`${artem.name} salary: ${artem.newSalary}`);
console.log(`${igor.name} salary: ${igor.newSalary}`);