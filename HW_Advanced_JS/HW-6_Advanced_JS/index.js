"use strict"

// Теоретичне питання
// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript?
// Вообще JS считается однопоточным кодом, а асинхронность позволяет обходить этот момент, неким образом доставая из стека
// части кода и выполняя их паралельно с текущим поток, а по мере выполнения, вкидывая их в конец очереди стеков вызова.


let btnSearch = document.querySelector('.btn-search');

btnSearch.addEventListener('click', async (ev) => {
	const getIp = await fetch('https://api.ipify.org/?format=json');
	const dataIp = await getIp.json();

	const getInfo = await fetch(`http://ip-api.com/json/${dataIp.ip}?fields=continent,country,regionName,city,district`);
	const dataInfo = await getInfo.json();

	new Card(dataInfo).render()
})


class Card {
	constructor(data) {
		this.continent = data.continent,
			this.country = data.country,
			this.district = data.district,
			this.regionName = data.regionName,
			this.city = data.city
	}

	render() {
		const container = document.querySelector('.container');

		container.innerHTML = `<div class="card mt-3" style="width: 18rem;">
			<div class="card-body">
			<h3 class="card-title">${this.continent}</h3>
			<h4 class="card-title">${this.country}</h4>
			<h6 class="card-subtitle mb-2 text-muted">${this.district}</h6>
			<h5 class="card-text">${this.regionName}</h5>
			<h5 class="card-text">${this.city}</h5>
		</div>
	</div>
	`
	}
}
