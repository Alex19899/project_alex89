"use strict"


async function getData() {
	const users = await fetch('https://ajax.test-danit.com/api/json/users');
	let dataUsers = await users.json();

	const posts = await fetch('https://ajax.test-danit.com/api/json/posts');
	let dataPosts = await posts.json();
	console.log(dataPosts);

	dataUsers.forEach(el => {
		dataPosts.forEach(elem => {
			if (el.id == elem.userId) {
				// console.log(elem);
				// console.log(el);
				new CreatePost(el, elem).render()
			}
		})
	});

	deletePost();
}

getData();



class CreatePost {
	constructor(users, posts) {
		this.user = users,
			this.post = posts
	}

	render() {
		const wrapper = document.querySelector('.wrapper');
		const div = document.createElement('div')
		div.classList.add('container-post')

		div.innerHTML = `
								<h2>${this.user.username}</h2>
								<h3>${this.user.name}</h3>
								<p>email: ${this.user.email}</p>
								<h4>${this.post.title}</h4>
								<p>${this.post.body}</p>
								<button type='button' class="btn-delete">DELETE</button>
							`
		div.setAttribute('id', this.post.id)
		wrapper.append(div);
	}
}


function deletePost() {
	let btn = document.querySelectorAll('.btn-delete');

	btn.forEach(el => {
		el.addEventListener('click', async (ev) => {
			const postId = ev.path[1].id;

			await fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
				method: 'DELETE'
			}).then((response) => {
				if (response.ok) {
					ev.path[1].remove();
				}
			})
				.catch((err) => alert('Помилка видалення'))
		})
	})
}


//  часть доп задания, красоту не наводил, сделал саму логику
let form = document.querySelector('.form')
form.addEventListener('submit', (ev) => {
	ev.preventDefault();
	fetch('https:ajax.test-danit.com/api/json/posts', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			userName: ev.target[0].value,
			name: ev.target[1].value,
			email: ev.target[2].value,
			title: ev.target[3].value,
			text: ev.target[4].value,
		})
	})
		.then((response) => {
			ev.target[0].value = "",
				ev.target[1].value = "",
				ev.target[2].value = "",
				ev.target[3].value = "",
				ev.target[4].value = ""
		})
		.catch((err) => alert('Помилка відправки даних'));
	form.classList.add('none');
	addPost.classList.remove('none');

	const wrapper = document.querySelector('.wrapper');
	const div = document.createElement('div')
	div.classList.add('container-post')

	div.innerHTML = `
								<h2>${ev.target[0].value}</h2>
								<h3>${ev.target[1].value}</h3>
								<p>email: ${ev.target[2].value}</p>
								<h4>${ev.target[3].value}</h4>
								<p>${ev.target[4].value}</p>
								<button type='button' class="btn-delete">DELETE</button>
							`
	div.setAttribute('id', 11)
	wrapper.prepend(div);
})


let addPost = document.querySelector('.add-post')
addPost.addEventListener('click', () => {
	addPost.classList.add('none');
	form.classList.remove('none');
});




