// "use strict" 

// Вопросы:
// 1. Опишіть своїми словами як працює метод forEach.
// Создается функция и с помощью метода forEach, перебирает значения массива и по очереди их подставляет в функцию
// которая их обрабатывает. 

// 2. Як очистити масив?
// length = 0 очистит весь массив, он станет пустым

// 3. Як можна перевірити, що та чи інша змінна є масивом?
// Array.isArray метод определения массивов




//                           ЕСТЬ ТРИ ВАРИАНТА РЕШЕНИЯ



// ______________________________________________________________ вариант 1

// const user = ["Alex", 1000, "Slav", "24.09.1989", 33, "Developer", null];

// function filterBy(arr, type) {
// 	const newUser = [];
// 	for (const elem of arr) {
// 		if (typeof elem !== type && elem !== null) {
// 			newUser.push(elem);
// 		}
// 	}
// 	return newUser;
// }
// console.log(filterBy(user, "number"));

// ____________________________________________________________ вариант 2


// const user = ["Alex", 1000, "Slav", "24.09.1989", 33, "Developer", null];

// let newUser = [];
// function filterBy(arr, type) {

// 	function filterValue(value) {

// 		if (typeof value !== type && value !== null) {
// 			newUser.push(value);
// 		}
// 		return newUser;
// 	}
// 	user.forEach(filterValue);

// 	return newUser;
// }
// console.log(filterBy(user, "number"));

// ___________________________________________________________________________ вариант 3

const user = ["Alex", 1000, "Slav", "24.09.1989", {}, [], 33, "Developer", null];

// function filterBy(arr, type) {
// 	return arr.filter(item => typeof item !== type && item !== null);
// }

const filterBy = (arr, typeData) => (
	arr.filter(elem => typeof (elem) !== typeData)
);


console.log(filterBy(user, "number"));