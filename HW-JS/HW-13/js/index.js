"use strict"

// 1. Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
// setInterval() запускается неоднократно, с заданным интервалом времени, если за это время функция
// не успевает обработаться, то запуски становятся в очередь в стек вызовов.
// setTimeout() запускается один раз, через указанный промежуток времени. Но ее можно вызвать рекурсивно, что хорошо для
// больших функций, тогда он будет работать аналогично сетинтервал, но не будет забивать стек вызовов и 
// ним можно более точно установить интервалы, так как не нужно учитывать вемя обработки функции.

// 2. Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
// Есть два нюанса, первый - это то, что сразу может и не выполниться, а выполниться в порядке очереди стека.
// А второй, это ограничение - первых 5 вызово выполняться сразу, а остальные будут идти с задержкой в 4 милисек


// 3. Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
// Что бы не переполнять стек вызовов.


// html и CSS специально не трогал)))))

window.addEventListener("load", function () {
	document.querySelector(".images-wrapper").style.display = "flex";
	
	let div = document.createElement("div");
	div.style.textAlign = "center";
	div.style.height = "40px";
	document.getElementsByTagName("body")[0].prepend(div);

	let buttonStop = document.createElement("button");
	buttonStop.innerText = "Stop";
	document.querySelector("div").append(buttonStop);

	let buttonStart = document.createElement("button");
	buttonStart.innerText = "Start";
	document.querySelector("div").append(buttonStart);


	let imgAll = document.querySelectorAll(".image-to-show");
	let slider = [];
	for(let i = 0; i < imgAll.length; i++) {
		slider[i] = imgAll[i].src;
		imgAll[i].remove();
	}

	let step = 0;
	function show () {
		let img = document.createElement("img");
		img.src = slider[step];
		img.classList.add("image-to-show");
		// ************fadeIn*******************
		setTimeout(() => {
			document.querySelector(".images-wrapper").appendChild(img);
			img.style.opacity = 0;
			img.style.transition =  `opacity 500ms`;
			setTimeout(() => {
				img.style.opacity = 1;
			}, 50);
		}, 500);
		// **********
		step + 1 == slider.length ? step = 0 : step++;
	};

	function change() {
		let img1 = document.querySelector(".image-to-show");
		// ************fadeOut*******************
		img1.style.opacity = 1;
		img1.style.transition =  `opacity 500ms`;
		img1.style.opacity = 0;
		setTimeout(() => {
			img1.remove();
		}, 500);
		// **********
		show ();
	};
	let interval = setInterval(change, 3000);
	show ();

	buttonStop.addEventListener("click", function () {
		run = false;
		clearInterval(interval);
		clearInterval(watch);
	});

	let run = false;
	buttonStart.addEventListener("click", function () {
		if(!run) {
			interval = setInterval(change, 3000);
			watch = setInterval(stopwatch, 5);
			count = 0;
		}
		run = true;
	});

	// _______________секундомер______________________
	let clock = document.createElement("div");
	clock.style.textAlign = "center";
	document.querySelector("div").append(clock);

	let count = 0;
	function stopwatch () {
		if(count > 3) {
			count = 0;
		} else {
			count += 0.005;
			clock.innerText = count.toFixed(3);
		}
	};
	let watch = setInterval(stopwatch, 5);
});
