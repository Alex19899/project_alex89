"use strict"

function showPassword() {
	let input = document.querySelectorAll("input");
	let icons = document.querySelectorAll(".fas");

	icons.forEach(el => el.addEventListener("click", function () {
		if (el.classList.contains("fa-eye")) {
			el.classList.remove("fa-eye");
			el.classList.add("fa-eye-slash");
		} else {
			el.classList.remove('fa-eye-slash');
			el.classList.add('fa-eye');
		}
	}));

	input.forEach(el => el.addEventListener("click", function () {
		if (el.getAttribute("type") === "password") {
			el.setAttribute("type", "text");
		} else {
			el.setAttribute("type", "password");
		}
	}));


	document.querySelector(".btn").addEventListener("click", function (event) {
		event.preventDefault();
		if (input[0].value === input[1].value) {
			document.querySelector(".error").remove();
			alert("You are welcome");
		} else {
			document.querySelector(".error").innerHTML = "Потрібно ввести однакові значення!";
		}
	});
}
showPassword()
