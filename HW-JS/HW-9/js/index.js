"use strict"

// 1. Опишіть, як можна створити новий HTML тег на сторінці.
// let clock = document.createElement("div");

// 2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
// Определяет позицию, куда добавить 
// 	beforebegin - перед тегом,
// 	afterbegin - сразу поле(перед первым ребенком),
// 	beforeend - перед закрывающим тегом 
// 	afterend - после закрывающего тега 


// 3. Як можна видалити елемент зі сторінки?
// clock.remove();


// ________________________________________обычное ДЗ
// const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];


// let list = document.createElement("ul");
// document.body.append(list) 

// function createList(array, place) {

// 	let arrlist = [];

// 	array.forEach(el => {
// 		arrlist.push(`<li>${el}</li>`)
// 	})
// 	return place.innerHTML = arrlist.join("");
// }

// createList(arr, list);


// ______________________________________доп задание___________________________________

const arr = [["Borispol", "Irpin"], "Kharkiv", "Kiev", "Odessa", "Lviv", "Dnieper",];

let list = document.createElement("ul");
document.body.append(list)

function createList(array) {

	let newArray = array.map(el => {

		if (!Array.isArray(el)) {
			return `<li>${el}</li>`;
		} else {
			return `<ul>${createList(el)}</ul>`;
		}
	}).join("");

	return newArray;
}

list.innerHTML = createList(arr);

let clock = document.createElement("div");
document.body.append(clock);
let count = 3;
clock.innerText = count;

setInterval(() => {
	if (count > 0) {
		count--;
		clock.innerText = count;
	}
}, 1000);

setTimeout(el => {
	list.remove();
	clock.remove();
}, 3000);

console.log(count);