"use strict"

window.addEventListener("load", function () {
	let ul = document.querySelector('.tabs');

	ul.addEventListener('click', function (ev) {
		document.querySelector('.active-p').classList.remove('active-p');
		document.querySelector('.active-tab').classList.remove('active-tab');
		let data = ev.target.dataset.tab;
		document.querySelector(`[id = ${data}]`).classList.add('active-p');
		ev.target.classList.add('active-tab')
	});


	// _____________________section id="work"_____radio-btn_______________

	const checkBoxs = document.forms.radio;
	let spanRadioBtn = document.querySelectorAll(".radio-text");
	const imgPush = document.getElementById("img-container");
	let loadMore = document.querySelector(".more");
	let imgArr = [];

	let images = {
		"graphic": [1, 2, 3, 4, 5, 6, 7, 8],
		"web": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
		"landing": [1, 2, 3, 4, 5, 6, 7],
		"wordpress": [1, 2, 3, 4, 5, 6, 7],
	}

	let loadingContainer = document.querySelector(".container-loading");
	checkBoxs.addEventListener("click", function (ev) {

		if (ev.target.tagName == "SPAN") {
			imgPush.innerHTML = "";
			loading();
			setTimeout(() => {
				spanRadioBtn.forEach(el => el.classList.remove("card_img-active"));
				ev.target.classList.add("card_img-active");
				loadMore.classList.remove("display-none");
				getContent(ev.target.id);
				loadingContainer.classList.add("display-none");
			}, 2000);
		}
		else if (!imgArr.length) {
			loadMore.classList.add("display-none");
		}
	})

	function loading() {
		loadingContainer.classList.remove("display-none");
	}

	function getContent(tabName) {
		imgArr = [];
		loadMore.classList.remove("display-none");
		if (tabName == "all") {
			for (let key in images) {
				images[key].forEach(el => {
					imgArr.push(`
				<div>
					<div class="card">
						<div>
							<img src="img/${key}/${key}${el}.jpg" class="card_img" alt="${key}${el}">
						</div>
						<div class="card_info">
							<img src="./img/icon.png" alt="icon.png">
							<h4>creative design</h4>
							<p>Web Design</p>
						</div>
					</div>
				</div>`);
				})
			}
		} else {
			images[tabName].forEach(el => {
				imgArr.push(`
			<div>
				<div class="card">
					<div>
						<img src="img/${tabName}/${tabName}${el}.jpg" class="card_img" alt="${tabName}${el}">
					</div>
					<div class="card_info">
						<img src="./img/icon.png" alt="icon.png">
						<h4>creative design</h4>
						<p>Web Design</p>
					</div>
				</div>
			</div>`);
			})
		}
		createHtml();
	}
	document.querySelector("#all").click();

	function createHtml() {
		for (let i = 0; i < 12; i++) {
			let text;
			if (imgArr.length) {
				text = imgArr.shift(i);
				imgPush.insertAdjacentHTML("beforeend", text);
				loadingContainer.classList.add("display-none");
				if (!imgArr.length || j == 3) {
					loadMore.classList.add("display-none");
				}
			}
		}
	}
	// ___________loadMore-btn______________
	let j = 0;
	loadMore.addEventListener("click", function () {
		if (imgArr.length) {
			loading();
			setTimeout(() => {
				createHtml();
			}, 2000);

		}
		if (!imgArr.length || j == 1) {
			loadMore.classList.add("display-none");
		}
		j++;
	})


	// ___________testimonial__________slider________________

	new Swiper('.testimonial-container', {
		effect: 'cube',
		speed: 650,
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev'
		},
		keyboard: {
			enabled: true,
			onlyInViewport: true,
			pageUpDown: true,
		},
		slidesPerView: 1,
		initialSlide: 1,
		grabCursor: true,
		slideToClickedSlide: true,
		centeredSlides: true,
		loop: true,

		thumbs: {
			swiper: {
				el: '.mini-testimonial-container',
				slidesPerView: 4,
				initialSlide: 1,
				slidesPerGroup: 2,
				slideToClickedSlide: true,
				loop: true,

			},
		},
	});

	// ____Gallery of best images______Masonry_________

	let grid = document.querySelector('.grid');
	const newMasonry = new Masonry(grid, {
		itemSelector: '.grid-item',
		gutter: 20,
	});


	let loadingMasonry = document.querySelector(".loading-masonry");
	let btnMasonry = document.getElementById("btn-masonry");
	btnMasonry.addEventListener("click", function () {
		loadingMasonry.classList.remove("display-none");

		setTimeout(() => {
			for (let i = 1; i < 12; i++) {
				createElemBig(i);
				append();
			}
			loadingMasonry.classList.add("display-none");
		}, 2000);
	})

	let div;
	function createElemBig(i) {
		div = document.createElement("div");
		div.classList.add("grid-item");
		div.innerHTML = `<img src="./img/masonry/load/${i}.jpg" alt="${i}">`;
	}

	function append() {
		grid.appendChild(div);
		newMasonry.appended(div);
		newMasonry.once('layoutComplete', function () {
			newMasonry.layout();
		});
		let moreImg = document.querySelector(".more-masonry");
		setTimeout(() => { moreImg.classList.add("display-none") }, 20)
	}

	let secret = document.querySelector(".img-search");
	secret.addEventListener("click", function () {
		let whatContainer = document.querySelector(".header-title");
		whatContainer.insertAdjacentHTML('beforebegin', `<div class="ba"><img src="./img/b/a/ka57094007.jpeg" alt="b"></div>`)

		setTimeout(() => {
			let whatSecret = document.querySelector(".ba");
			whatSecret.remove();
		}, 2000);
	})
})
