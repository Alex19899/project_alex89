// 1. Опишіть своїми словами що таке Document Object Model (DOM)
	// DOM это дерево обьектов, которое содержат в себе все (структуру, стили, содержимое)ю Благодаря DOM,
	// у нас есть возможность динамически менять или дополнять все в DOM

// 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
	// innerText возьмет из указаного обьекта только текст, даже если внутри него будут еще другие теги.
	// А innerHTML покаже все что есть внутри, вместе с тегами и текстом

// 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
	// Способов обратиться есть много, как прямых так и косвенных (типа первый и последний ребенок),
	// но я для себя выделил два основных на мой взгляд более удобных, это обращение по селектору и по тегу.
	// Вот эти два метода считаю лучшими.
	// А использовать document.querySelector() или document.querySelectorAll() это зависит от нашей цели.


// Завдання:

// 1. Знайти всі параграфи на сторінці та встановити колір фону #ff0000

// const paragraf = document.querySelectorAll("p");
// paragraf.forEach(elem => elem.style.backgroundColor = "#ff0000");



// 2. Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль.
// Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
// вторая часть условия очень плохо написанна, вообще не понятно, что хотят, гадали всей группой

// const element = document.getElementById("optionsList");
// console.log(element);

// // или так
// const parent = element.parentNode;
// console.log(parent);
// console.log(element.childNodes);

// или так нужно было?
// for (let el of element.childNodes) {
// 	// console.log(el);
// 	console.log(el);
// }




// 3. Встановіть в якості контента елемента з класом testParagraph наступний параграф - 
// This is a paragraph

// const firstParagraf = document.querySelector("#testParagraph");
// firstParagraf.innerHTML = "This is a paragraph";



// 4. и 5 Отримати елементи , вкладені в елемент із класом main-header і вивести їх у консоль.
// Кожному з елементів присвоїти новий клас nav-item.

const header = document.querySelector(".main-header");
let headerChildren = header.children
// console.log(headerChildren);

for (let el of headerChildren){
	el.classList.add("nav-item");
	console.log(el);
}




// 6. Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

const elements = document.querySelectorAll(".section-title");
console.log(elements);

for (let el of elements) {
	el.classList.remove("section-title");
	console.log(el);
}
