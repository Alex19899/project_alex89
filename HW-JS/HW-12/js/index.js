"use strict" 

// Чому для роботи з input не рекомендується використовувати клавіатуру?
	// Есть много способов ввода в поле инпут и по этому отслеживать только события клавиатуры,
	// это значит упускать все остальные. Более правильным считается отслеживать события в инпут,
	// с помощью специального события input.

window.addEventListener("keydown", function(event) {
	
	let btn = document.querySelectorAll(".btn");
	btn.forEach(el => {
		if(event.key.toUpperCase() === el.innerHTML.toUpperCase()) {
			el.style.backgroundColor = "blue";
		} else {
			el.style.backgroundColor = "";
		}
	})
})


