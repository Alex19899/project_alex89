'use strict'

const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const clean = require('gulp-clean');
const purgecss = require('gulp-purgecss');
const uglify = require('gulp-uglify');
const imagemin = require('gulp-imagemin');
const browserSync = require('browser-sync').create();
const watch = require('gulp-watch');


// -----------------turn---------------

const turnJs = ['./src/js/header/header.js','./src/js/main/main.js', './src/js/test1.js', './src/js/test2.js']


// ---------build-----------------
gulp.task('clear', function () {
	return gulp.src('dist/*', { read: false })
		.pipe(clean());
})


gulp.task('clearCSS', function () {
	return gulp.src('dist/css/**/*', { read: false })
		.pipe(clean());
})


gulp.task('styles', function () {
	return gulp.src('./src/scss/style.scss')
		.pipe(sass())
		// .pipe(purgecss({
		// 	content: ['*.html']
		// }))
		.pipe(autoprefixer({
			overrideBrowserslist: ['>0.01%'],
			cascade: false
		}))
		.pipe(concat('main.min.css'))
		.pipe(cleanCSS({
			level: 2
		}))
		.pipe(gulp.dest('./dist/css'))
		.pipe(browserSync.stream());
})

gulp.task('js', function () {
	return gulp.src(turnJs)
		.pipe(concat('main.min.js'))
		.pipe(uglify({
			toplevel: true
		}))
		.pipe(gulp.dest('./dist/js'))
		.pipe(browserSync.stream());
})

gulp.task('img', function () {
	return gulp.src('./src/img/**/*')
		.pipe(imagemin())
		.pipe(gulp.dest('./dist/img'))
})


// -------------------dev----------------
gulp.task('dev', function () {
	browserSync.init({
		server: {
			baseDir: "./"
		}
	});
	
	
	
	gulp.watch('./src/scss/**/*.scss', gulp.series('styles'));
	gulp.watch('./src/js/**/*js', gulp.series('js'));
	gulp.watch("./*.html").on('change', browserSync.reload);
})

exports.build = gulp.series('clear', gulp.parallel('styles', 'js', 'img'));
exports.dev2 = gulp.series('clearCSS', 'dev')
