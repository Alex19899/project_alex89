// "use strict"

document.addEventListener("DOMContentLoaded", function() {
	let headerBurger = document.querySelector(".header__burger");
	let headerMenu = document.querySelector(".header__menu");
	let headerItem = document.querySelectorAll(".header__item");
	


	headerBurger.addEventListener("click", function() {
		headerBurger.classList.toggle("active");
		headerMenu.classList.toggle("active");
		headerItem.forEach(element => {
			element.classList.toggle("active");
		});
	})
})