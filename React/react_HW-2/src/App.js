import { React, Component } from 'react';
// import { useState } from 'react';
import Header from './component/Header/Header';
import Modal from './component/Modal/Modal';
import Button from './component/Button/Button';
import ProductList from './component/ProductList/ProductList';
import './App.scss';



export default class App extends Component {
  constructor(props) {
    super(props);


    this.state = {
      modalActive: false,
      products: [],
      prodFavorite: JSON.parse(localStorage.getItem('prodFavorite')) == null ? [] : JSON.parse(localStorage.getItem('prodFavorite')),
      favoriteColor: false,
      cartProduct: JSON.parse(localStorage.getItem('cartProduct')) == null ? [] : JSON.parse(localStorage.getItem('cartProduct')),
      checkAdd: ''
    }
  }

  async componentDidMount() {
    await fetch('./data/data.json')
      .then(res => res.json())
      .then(data => {
        this.setState({
          products: data.products,
        })
      })
  }


  activeModal = (active) => {
    this.state.modalActive ? this.setState({ modalActive: false }) : this.setState({ modalActive: true });
  } 

  // ----------add product to cart-----------------
  check = (product) => {
    this.setState({ checkAdd: product })
  }

  addToCart = (product) => {
    let prod = [];
    if (!!localStorage.getItem('cartProduct')) {
      prod = [...JSON.parse(localStorage.getItem('cartProduct'))];
    }
    prod.push(product);
    localStorage.setItem('cartProduct', JSON.stringify(prod))
    this.setState({ cartProduct: prod })
  }
// ---------------end--------------
  // ------------star------------
  favoriteColor = () => {
		this.state.favoriteColor ? this.setState({ favoriteColor: false }) : this.setState({ favoriteColor: true })
	}

  addToFavorite = (product) => {
    console.log(product);
    let prod = [];
    if (!!localStorage.getItem('prodFavorite')) {
      prod = [...JSON.parse(localStorage.getItem('prodFavorite'))];
    }
    prod.push(product);
    localStorage.setItem('prodFavorite', JSON.stringify(prod))
    this.setState({ prodFavorite: prod })
  }

  removeFavorite = (product) => {
    let arr = this.state.prodFavorite;
    let newArr = arr.filter(item => item.id !== product.id ? true : false);
    localStorage.setItem('prodFavorite', JSON.stringify(newArr))
    this.setState({prodFavorite: newArr})
  }
// --------------end------------------

  render() {
    
    return (
      <>
        <Header
          favorite={this.state.prodFavorite} 
          cartProduct={this.state.cartProduct}
          key='header'
        />

        {this.state.modalActive && (
          <Modal
            key='Modal'
            active={this.state.modalActive}
            close={this.activeModal}
            header=''
            text='You wont add product to cart?'
            actions={
              <>
                <Button 
                  key={'Button'}
                  text='Ok'
                  bg={{ backgroundColor: 'rgb(251, 251, 251)' }}
                  onClick={() => {
                    this.activeModal();
                    this.addToCart(this.state.checkAdd);
                  }}/>,
                <Button 
                  key={'Button-02'}
                  text='Cancel' 
                  bg={{ backgroundColor: 'rgb(251, 251, 251)' }} 
                  onClick={this.activeModal}/>
                </>}
          />)
        }
        <div className='flex'>
          <div className='flex container'>
            <ProductList 
              key={'ProductList'}
              products={this.state.products}
              showModal={this.activeModal}
              removeFromFavorites={this.removeFavorite}
              addProductToCart={this.check}
              favoriteColorActive={this.state.favoriteColor}
              addFavorite={this.addToFavorite} />
          </div>
          {/* <div>
            <div>cart</div>
            <div>favorite</div>
          </div> */}
        </div>
      </>
    );
  }
}

