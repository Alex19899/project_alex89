import { Component } from "react";
import { FaStar } from "react-icons/fa";
import './Favorite.scss';

export default class Favorite extends Component {

	

	render() {
		// console.log('fav-  ' + this.props.favorite);
		return (
			<div className="wrapper-logo-favorite">
				<h3 className="header-favorite">Favorite</h3>
				<FaStar className={this.props.favorite.length > 0 ? "favorite-logo active" : "favorite-logo"}/>
				
				{this.props.favorite.length > 0 ? 
					<p className="favorite-cart">{this.props.favorite.length}</p>
					: false
					}
				
			</div>
			
		)
	}
}