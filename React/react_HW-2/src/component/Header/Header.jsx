import { Component } from "react";
import Cart from "../Cart/Cart";
import Favorite from "../Favorite/Favorite";
import './Header.scss'



export default class Header extends Component {


	render() {
		return (
			<>
				<div className="flex-start header">
					<img src="./img/Alex-logo.png" alt="logo"/>
					<h1>my-SHOP</h1>
					<div className="flex">
						<Cart cartProduct={this.props.cartProduct}/>
						<Favorite favorite={this.props.favorite}/>
					</div>
				</div>	
			</>
		)
	}
}