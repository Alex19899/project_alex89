import { Component } from "react";
import ProductItem from "../ProductItem/ProductItem";
import './productList.scss'


export default class ProductList extends Component {


	render() {
		// console.log(this.props.products);
		return (
			<>
				{this.props.products.map(el => {
					return (
						<div className="product-wrapper" key={'list' + el.id}>
							<ProductItem product={el} 
							color={el.color}
							key={el.id} 
							removeFromFavorites={(productRemove) => this.props.removeFromFavorites(productRemove)}
							favoriteColorActive={this.props.favoriteColorActive}
							addFavorite={(productToFavorite) => this.props.addFavorite(productToFavorite)}
							showModalList={(active) => this.props.showModal(active)}
							addProductToCart={(productToCart) => this.props.addProductToCart(productToCart)}
							/>
						</div>
						)})}	
			</>
		)
	}
}