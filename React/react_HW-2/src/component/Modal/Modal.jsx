import {React, Component} from "react";
import './modal.scss'

export default class Modal extends Component {


	render() {
		return (
			<div className={this.props.active ? 'modal active' : 'modal'} onClick={this.props.close}>
				<div className="modal__content" onClick={(e) => e.stopPropagation()}>
					<div className="modal__header">
						<h3>{this.props.header}</h3>
						<h3 className="modal__header-close-btn" onClick={this.props.close}>x</h3>
					</div>
					<p>{this.props.text}</p>
					<div className="modal__container-btn">
					{this.props.actions}
					</div>
				</div>
			</div>
		)
	}
}