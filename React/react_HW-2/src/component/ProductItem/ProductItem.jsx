import { Component } from "react";
import Button from "../Button/Button";
import ColorProduct from "../ColorProduct/ColorProduct";
import { FaStar } from "react-icons/fa";
import './productItem.scss';
import '../../App.scss'

export default class ProductItem extends Component {
	constructor(props) {
		super(props);

		this.state = {
			activeFavorite: false,
		}
	}

	componentDidMount() {
		let fav = JSON.parse(localStorage.getItem('prodFavorite')) == null ? [] : JSON.parse(localStorage.getItem('prodFavorite'));
		fav.filter(el => {
			if(el.id == this.props.product.id ) {
				this.setState({ activeFavorite: true })
			} 
		})
	}

	activeStar = () => {
		if (this.state.activeFavorite) {
			this.setState({ activeFavorite: false })
			this.props.removeFromFavorites(this.props.product);
		} else {
			this.setState({ activeFavorite: true })
		}
	}

	render() {
		return (
				<div className="product-item" key={this.props.product.id}>
					<p className="imem-code">code: {this.props.product.id}</p>
					<img src={this.props.product.img_url} alt='qsx'/>
					<h4>{this.props.product.name}</h4>
					<p>{this.props.product.decription}</p>

					<div className="flex-star" onClick={() => {
						this.props.addFavorite(this.props.product); 
						this.activeStar()
						}} >
						<FaStar className={this.state.activeFavorite ? "star-active" : "favorite"}/>
						<h6>my favorite</h6>
					</div>
					<div> 
						<ul className="color-list">
							{this.props.color.map(item => <ColorProduct key={item} bg={{ backgroundColor: item }} />)}
						</ul>
					</div>
					         
								
					<div className="product-item-footer flex">
						<h3 >$: {this.props.product.price}</h3>

						<Button
							text='ADD TO CART' 
							productBtn={this.props.product.id}
							bg={{ backgroundColor: 'rgb(234, 232, 232)'}} 
							onClick={() => { 
								this.props.showModalList(true);
								this.props.addProductToCart(this.props.product);
								}} />
					</div>
				</div>
		)
	}
}