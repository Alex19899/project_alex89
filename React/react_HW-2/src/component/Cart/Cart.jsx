import { Component } from "react";
import './Cart.scss';
import { FaCartArrowDown } from "react-icons/fa";


export default class Cart extends Component {

	render() {
		return (
			<div className="wrapper-logo-cart">
				<h3>Cart</h3>
				<FaCartArrowDown className={this.props.cartProduct.length > 0 ? "cart-logo active" : "cart-logo"}/>
				{this.props.cartProduct.length > 0 ? 
					<p className='shop-cart'>{this.props.cartProduct.length}</p>
					: false
					}
			</div>
			
			
		)
	}
}