import * as yup from 'yup';

const notNumber = /^[^0-9]*$/;

export const validationSchema = yup.object({
	firstName: yup
		.string()
		.matches(notNumber, "The field must not contain numbers")
		.max(15, 'Must be 15 characters or less')
		.required('Required'),
	lastName: yup
		.string()
		.matches(notNumber, "The field must not contain numbers")
		.max(20, 'Must be 20 characters or less')
		.required('Required'),
	age: yup
		.number()
		.typeError("enter your age")
		.positive("enter your age")
		.integer("enter your age")
		.max(110, 'enter your age')
		.required("Required"),
	adress: yup
		.string()
		.min(5, 'Must be minimum 5 characters')
		.max(60, 'Must be 60 characters or less')
		.required('Required'),
	phone: yup
		.number()
		.typeError("That doesn't look like a phone number")
		.positive("A phone number can't start with a minus")
		.integer("A phone number can't include a decimal point")
		.min(8)
		.required("Required"),
})
