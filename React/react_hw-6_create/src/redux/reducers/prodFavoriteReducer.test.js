import { prodFavoriteReducer, GET_FAVORITE_PRODUCT } from './prodFavoriteReducer';

describe('prodFavoriteReducer', () => {
	it('should return the default state', () => {
		const state = prodFavoriteReducer(undefined, {});
		expect(state).toEqual({ prodFavorite: [] });
	});

	it('should update the state when GET_FAVORITE_PRODUCT is dispatched', () => {
		const prodFavorite = ['product1', 'product2'];
		const action = {
			type: GET_FAVORITE_PRODUCT,
			payload: { prodFavorite },
		};
		const state = prodFavoriteReducer(undefined, action);
		expect(state).toEqual({ prodFavorite });
	});
});