import { productsReducer, GET_PRODUCTS_SUCCESS } from './productsReducer';

describe('productsReducer', () => {
	it('should return the default state', () => {
		const state = productsReducer(undefined, {});
		expect(state).toEqual({ products: [] });
	});

	it('should update the state when GET_PRODUCTS_SUCCESS is dispatched', () => {
		const action = { type: GET_PRODUCTS_SUCCESS, payload: { products: ['product1', 'product2'] } };
		const state = productsReducer(undefined, action);
		expect(state).toEqual({ products: ['product1', 'product2'] });
	});
});