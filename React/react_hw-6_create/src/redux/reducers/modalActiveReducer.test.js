import { modalActiveReducer, MODAL_IS_ACTIVE, MODAL_IS_NOT_ACTIVE, MODAL_CART_IS_ACTIVE, MODAL_CART_IS_NOT_ACTIVE } from './modalActiveReducer';

describe('modalActiveReducer', () => {
	it('should return the default state', () => {
		const state = modalActiveReducer(undefined, {});
		expect(state).toEqual({ modalActive: false, isActiveModalCart: false });
	});

	it('should update the state when MODAL_IS_ACTIVE is dispatched', () => {
		const action = { type: MODAL_IS_ACTIVE };
		const state = modalActiveReducer(undefined, action);
		expect(state).toEqual({ modalActive: true, isActiveModalCart: false });
	});

	it('should update the state when MODAL_IS_NOT_ACTIVE is dispatched', () => {
		const action = { type: MODAL_IS_NOT_ACTIVE };
		const state = modalActiveReducer(undefined, action);
		expect(state).toEqual({ modalActive: false, isActiveModalCart: false });
	});

	it('should update the state when MODAL_CART_IS_ACTIVE is dispatched', () => {
		const action = { type: MODAL_CART_IS_ACTIVE };
		const state = modalActiveReducer(undefined, action);
		expect(state).toEqual({ modalActive: false, isActiveModalCart: true });
	});

	it('should update the state when MODAL_CART_IS_NOT_ACTIVE is dispatched', () => {
		const action = { type: MODAL_CART_IS_NOT_ACTIVE };
		const state = modalActiveReducer(undefined, action);
		expect(state).toEqual({ modalActive: false, isActiveModalCart: false });
	});
});