import { cartProductReducer, GET_CART_PRODUCT } from './cartProductReducer';

describe('cartProductReducer', () => {
	it('should return the default state', () => {
		const state = cartProductReducer(undefined, { type: 'TEST_ACTION' });
		expect(state).toEqual({
			cartProduct: [],
		});
	});

	it('should handle GET_CART_PRODUCT', () => {
		const cartProduct = [{ id: 1, name: 'Product 1', price: 10 }];
		const state = cartProductReducer(undefined, {
			type: GET_CART_PRODUCT,
			payload: { cartProduct },
		});
		expect(state).toEqual({
			cartProduct: cartProduct,
		});
	});
});