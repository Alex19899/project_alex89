import React from 'react';
import '@testing-library/jest-dom';
import { render, fireEvent } from '@testing-library/react';
import { Modal } from './Modal';

describe('Modal component', () => {
	test('renders modal when active is true', () => {
		const props = {
			active: true,
			close: jest.fn(),
			header: 'Test Header',
			text: 'Test Text',
			actions: <button>Test Button</button>,
		};
		const { getByText } = render(<Modal {...props} />);
		const headerElement = getByText(props.header);
		const textElement = getByText(props.text);
		const buttonElement = getByText('Test Button');

		expect(headerElement).toBeInTheDocument();
		expect(textElement).toBeInTheDocument();
		expect(buttonElement).toBeInTheDocument();
	});

	test('calls close function when clicking on the close button', () => {
		const props = {
			active: true,
			close: jest.fn(),
			header: 'Test Header',
			text: 'Test Text',
			actions: <button>Test Button</button>,
		};
		const { getByTestId } = render(<Modal {...props} />);
		const closeButtonElement = getByTestId('close-button');

		fireEvent.click(closeButtonElement);

		expect(props.close).toHaveBeenCalled();
	});
});