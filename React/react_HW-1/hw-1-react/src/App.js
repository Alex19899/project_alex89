import './App.scss';
import { Component } from 'react';
import Modal from './component/Modal/Modal';
import Button from './component/Button/Button';




export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      modalActive1: false,
      modalActive2: false
    }
  }


  render() {
    return (
      <>
        <Button 
          idButton='1'
          text='Open first modal' 
          onClick={() => {this.setState({modalActive1: true})}}
          bg={{backgroundColor: 'yellowgreen'}}
        />
        <Button 
          text='Open second modal'
          onClick={() => {this.setState({modalActive2: true})}}
          bg={{backgroundColor: 'burlywood'}}
        />
        
        

        <Modal 
          idModal='1'
          active={this.state.modalActive1} 
          close={() => {this.setState({modalActive1: false})}}
          header='TITLE MODAL 1'
          text='Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.'
          actions={[
          <Button text='Ok' bg={{backgroundColor: 'rgb(88, 82, 173)'}} onClick={() => {this.setState({modalActive1: false})}}/>, 
          <Button text='Cancel' bg={{backgroundColor: 'rgb(189, 75, 183)'}} onClick={() => {this.setState({modalActive1: false})}}/>]}
          />

          <Modal 
          active={this.state.modalActive2} 
          close={() => {this.setState({modalActive2: false})}}
          header='TITLE MODAL 2'
          text='Наше дело не так однозначно, как может показаться: выбранный нами инновационный путь не даёт нам иного выбора, кроме определения экспериментов, поражающих по своей масштабности и грандиозности. Вот вам яркий пример современных тенденций — перспективное планирование выявляет срочную потребность первоочередных требований.'
          actions={[
          <Button text='Ok' bg={{backgroundColor: 'rgb(88, 82, 173)'}} onClick={() => {this.setState({modalActive2: false})}}/>, 
          <Button text='Cancel' bg={{backgroundColor: 'rgb(189, 75, 183)'}} onClick={() => {this.setState({modalActive2: false})}}/>]}
          />
      </>
    );
  }
}

