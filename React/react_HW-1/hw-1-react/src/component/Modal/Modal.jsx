import {React, Component} from "react";
import Button from "../Button/Button";
import './modal.scss'

export default class Modal extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		console.log(this.props.close);
		return (
			<div className={this.props.active ? 'modal active' : 'modal'} onClick={this.props.close}>
				<div className="modal__content" onClick={(e) => e.stopPropagation()}>
					<div className="modal__header">
						<h3>{this.props.header}</h3>
						<h3 className="modal__header-close-btn" onClick={this.props.close}>x</h3>
					</div>
					<p>{this.props.text}</p>
					<div className="modal__container-btn">
					{this.props.actions}
					</div>
				</div>
			</div>
		)
	}
}