import { Component } from "react";
import './button.scss';

export default class Button extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		console.log(this.props.show);
		return (
			<button 
				onClick={this.props.onClick}
				style={this.props.bg}
				className='btn'
				>{this.props.text}</button>
		)
	}
}