import React, { useEffect, useState } from 'react';
import { Outlet } from 'react-router-dom';
import Header from './component/Header/Header';
import './root.scss';

const Root = () => {
	const [cartProduct, setCartProduct] = useState(JSON.parse(localStorage.getItem('cartProduct')) == null ? [] : JSON.parse(localStorage.getItem('cartProduct')));
	const [prodFavorite, setProdFavorite] = useState(JSON.parse(localStorage.getItem('prodFavorite')) == null ? [] : JSON.parse(localStorage.getItem('prodFavorite')));
	const [products, setProducts] = useState([]);
	const [prodForCart, setProductForCart] = useState([])


	useEffect(() => {
		fetch('./data/data.json')
			.then(response => response.json())
			.then(data => {
				setProducts(data.products);

			})
			.catch(error => console.error(error));
	}, [])


	const addToCart = (productId) => {
		let obj = {}

		if (cartProduct[productId]) {
			obj = {
				...cartProduct,
				[productId]: cartProduct[productId] + 1
			}
			setCartProduct(obj)

		} else {
			obj = {
				...cartProduct,
				[productId]: 1
			}
			setCartProduct(obj)
		}
		localStorage.setItem('cartProduct', JSON.stringify(obj))
	}
	// ---------------end--------------

	// ------------favorite------------
	const addToFavorite = (product) => {
		// console.log(product);
		product.isActiveFavorite = true;
		let prod = [];
		if (!!localStorage.getItem('prodFavorite')) {
			prod = [...JSON.parse(localStorage.getItem('prodFavorite'))];
		}
		prod.push(product);
		localStorage.setItem('prodFavorite', JSON.stringify(prod))
		setProdFavorite(prod);
	}

	const removeFavorite = (product) => {
		let arr = prodFavorite;
		let newArr = arr.filter(item => item.id !== product.id ? true : false);
		localStorage.setItem('prodFavorite', JSON.stringify(newArr))
		setProdFavorite(newArr);
	}
	// --------------end------------------

	return (
		<>
			<Header
				favorite={prodFavorite}
				cartProduct={cartProduct}
				key='header'
			/>
			<Outlet
				context={{
					addToCart,
					addToFavorite,
					removeFavorite,
					cartProduct,
					prodFavorite,
					products,
					cartProduct,
					setCartProduct
				}}

			/>
		</>
	)
}


export default Root;

