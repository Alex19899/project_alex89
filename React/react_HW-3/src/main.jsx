import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.scss';
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import CartPage from './component/page/CartPage/CartPage';
import FavoritePage from './component/page/FavoritePage/FavoritePage';
// import Header from './component/Header/Header';
import Root from './Root';
import Home from './component/Home/Home';



const router = createBrowserRouter([
  {
    path: "/",
    element: <Root/>,
    errorElement: <div>Not Found</div>,
    children: [
      {
        path: "/",
        element: <Home />,
      },
      {
        path: "/cart",
        element: <CartPage />,
      },
      {
        path: "favorite",
        element: <FavoritePage />,
      },
    ],
  },
]);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);


