import { React, useEffect, useState } from 'react';
import Modal from '../Modal/Modal';
import Button from '../Button/Button';
import ProductList from '../ProductList/ProductList';
import './home.scss'
import { useOutletContext } from 'react-router-dom';



const Home = () => {

	const {addToCart, addToFavorite, removeFavorite, products} = useOutletContext();
	const [modalActive, setModalActive] = useState(false);
	const [checkAdd, setCheckAdd] = useState('');
	const [favoriteColor, setFavoriteColor] = useState(false);


	const activeModal = (active) => {
		modalActive ? setModalActive(false) : setModalActive(true);
	}

// ----------add product to cart-----------------
	const check = (product) => setCheckAdd(product);


// ------------favorite------------
	const addFavoriteColor = () => {
		favoriteColor ? setFavoriteColor(false) : setFavoriteColor(true);
	}

// --------------end------------------


	return (

		<>
			{modalActive && (
				<Modal
					key='Modal'
					active={modalActive}
					close={activeModal}
					header=''
					text='You wont add product to cart?'
					actions={
						<>
							<Button
								key={'Button'}
								text='Ok'
								bg={{ backgroundColor: 'rgb(251, 251, 251)' }}
								onClick={() => {
									activeModal();
									addToCart(checkAdd);
								}} />
							<Button
								key={'Button-02'}
								text='Cancel'
								bg={{ backgroundColor: 'rgb(251, 251, 251)' }}
								onClick={activeModal} />
						</>}
				/>)
			}
			<div className='flex'>
				<div className='container'>
					<ProductList
						key={'ProductList'}
						products={products}
						showModal={activeModal}
						removeFromFavorites={removeFavorite}
						addProductToCart={check}
						favoriteColorActive={addFavoriteColor}
						addFavorite={addToFavorite} />
				</div>
			</div>
		</>
	);
}


export default Home;

