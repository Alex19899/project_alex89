import ProductItem from "../ProductItem/ProductItem";
import './productList.scss'



const ProductList = (props) => {
	const { products, showModal, removeFromFavorites, addProductToCart, favoriteColorActive, addFavorite } = props;
	return (
		<div className="flex">
			{products.map(product =>
				<div className="product-wrapper" key={product.id}>
					<ProductItem
						product={product}
						color={product.color}
						removeFromFavorites={(productRemove) => removeFromFavorites(productRemove)}
						favoriteColorActive={favoriteColorActive}
						addFavorite={(productToFavorite) => addFavorite(productToFavorite)}
						showModalList={(active) => showModal(active)}
						addProductToCart={(productToCart) => addProductToCart(productToCart)}
					/>
				</div>
			)}
		</div>
	)
}

// ProductList.propTypes = {
// 	removeFromFavorites: PropTypes.func,
// 	addFavorite: PropTypes.func
// };

export default ProductList;

