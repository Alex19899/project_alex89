import { useNavigate, Link, Outlet } from "react-router-dom";
import Cart from "../CartLogo/CartLogo";
import Favorite from "../FavoriteLogo/FavoriteLogo";
import './header.scss'


const Header = ({ favorite, cartProduct }) => {
	const navigate = useNavigate();
	return (
		<>
			<div className="flex-start header">
				<Link to="/">
					<img src="./img/Alex-logo.png" alt="logo" />
				</Link>
				<h1
					onClick={() => {
					navigate("/");
				}}
				>my-SHOP</h1>
				<div className="flex">
					<Cart cartProduct={cartProduct} />
					<Favorite favorite={favorite} />
				</div>
			</div>
		</>
	)
}

export default Header;