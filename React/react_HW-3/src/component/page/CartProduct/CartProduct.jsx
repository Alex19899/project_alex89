import React, { useState } from 'react';
import InputCartProduct from './InputCartProduct/InputCartProduct';
import './cartProduct.scss';

const CartProduct = ({ products, quantity, productId, cartProduct, setCartProduct }) => {
	const [amount, setAmount] = useState(quantity);
	

	const removeProduct = () => {
		let newCartProduct = {...cartProduct};
		delete newCartProduct[productId];
		setCartProduct(newCartProduct);
		localStorage.setItem('cartProduct', JSON.stringify(newCartProduct))
		
	}


	return (
		<div className='wrapper-product'>
			{products.map(product => (
				productId == product.id ?
					<>
						<div className='wrapper-item'>
							<img src={product.img_url} alt={product.name} />
							<h5>{product.name}</h5>
						</div>
						<h5>{product.price}</h5>

						<InputCartProduct 
							key={`inp ${product.id}`}
							removeProduct={removeProduct}
							amount={amount}
							setAmount={setAmount}
							cartProduct={cartProduct}
							setCartProduct={setCartProduct}
							productId={productId}
							/>

						<h5>{product.price * amount}</h5>
					</>
					: false)
			)}
			<button onClick={() => removeProduct()}>Delete</button>
		</div>
	)
}

export default CartProduct