
import './inputCartProduct.scss'

const InputCartProduct = ({ productId, cartProduct, setCartProduct, amount, setAmount, removeProduct }) => {

	const amountProducts = (event) => {
		const newValue = parseInt(event.target.value);
		let obj = {}
		

		if (cartProduct[productId] && newValue > amount) {
			obj = {
				...cartProduct,
				[productId]: cartProduct[productId] + 1 
			}
			setCartProduct(obj)
		} else if (cartProduct[productId] && newValue < amount) {
			obj = {
				...cartProduct,
				[productId]: cartProduct[productId] - 1 
			}
			setCartProduct(obj)
		} else {
			obj = {
				...cartProduct,
				[productId]: 1
			}
			setCartProduct(obj)
		}
		localStorage.setItem('cartProduct', JSON.stringify(obj))
		setAmount(newValue);
		event.target.value <= 0 ? removeProduct() : false;
	}

	return (
		<>
			<div className='wrapper-input-product'>
				<input className='input-product'
					onChange={(event) => (
						 amountProducts(event))}
					type='number'
					step="1"
					min="0"
					autoComplete="off."
					value={amount}
				></input>
			</div>
		</>
	)
}

export default InputCartProduct