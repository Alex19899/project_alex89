import React from 'react'
import { FaStar } from "react-icons/fa";
import '../../ProductList/productList.scss'

const FavoriteProduct = ({ product, removeFavorite }) => {
	return (
		<div>
			<div className='wrapper-product'>

				<div className='wrapper-item'>
					<img src={product.img_url} alt={product.name} />
					<h5>{product.name}</h5>
				</div>
				<h5>{product.price}</h5>


				<FaStar
					className={product.isActiveFavorite ? "star-active" : "favorite"}
					onClick={() => removeFavorite(product)}
				/>

				{/* <button onClick={() => removeProduct()}>Delete</button> */}
			</div>
		</div>
	)
}

export default FavoriteProduct