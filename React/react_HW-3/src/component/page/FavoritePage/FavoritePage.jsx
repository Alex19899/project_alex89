import React from 'react'
import { useOutletContext } from 'react-router-dom';
import FavoriteProduct from './FavoriteProduct';

const FavoritePage = () => {
	const {prodFavorite, removeFavorite} = useOutletContext()

	// console.log(prodFavorite);

	return (
		<div className='container cart-page-wrapper'>
		<h2>MY FAVORITE</h2>

		<div className='flex cart-page__header'>
			<h5>ITEM</h5>
			<h5>PRICE</h5>
			<h5>REMOVE</h5>
		</div>

		{
			prodFavorite.map(el => (
				<FavoriteProduct 
				product={el} 
				removeFavorite={removeFavorite}
				/>

			))
		}

		<div className='flex'>
			<h5>total</h5>
			<button>BUY</button>
		</div>
	</div>

	)
}

export default FavoritePage;