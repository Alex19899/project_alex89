import { useOutletContext } from 'react-router-dom';
import CartProduct from '../CartProduct/CartProduct';
import './cartPage.scss'

const CartPage = () => {

	const { cartProduct, products, setCartProduct} = useOutletContext();

	return (
		<div className='container cart-page-wrapper'>
			<h2>SOPING CART</h2>

			<div className='flex cart-page__header'>
				<h5>ITEM</h5>
				<h5>PRICE</h5>
				<h5>QUANTITY</h5>
				<h5>TOTAL</h5>
				<h5>REMOVE</h5>
			</div>

			{
				Object.entries(cartProduct).map(([productId, quant]) =>
				
					<CartProduct
						cartProduct={cartProduct}
						setCartProduct={setCartProduct}
						products={products}
						quantity={quant}
						productId={productId}
						key={`cart ${productId}`} />
				)
			}

			<div className='flex'>
				<h5>total</h5>
				<button>BUY</button>
			</div>
		</div>
	)
}

export default CartPage;